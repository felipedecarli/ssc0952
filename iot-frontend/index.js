var activeTable;
window.onload = function () {
  setupLogsTable();
  setLogsList().then((result) => {
    //setupNormalizedSearch("logs");
    console.log("Tabela de logs atualizada!");
  });
  $("#sendCommand").click(function () {
    getCommand();
  });
};

function getSensorUserInput() {
  //Pegamos a lista de sensores
  getSensors()
    .then((sensorsResult) => {
      //Pegamos a seleção do usuário
      const modalSensorsConfig = getSearchableModalConfig(
        "sensorList",
        afpResult.data,
        "nameAFP",
        "Selecione o sensor",
        "sensorSelected",
        true,
        false,
        true
      );

      showModal(modalSensorsConfig, normalWidthModal)
        .then((sensorSelection) => {
          setLogsList(sensorSelection).then((result) => {
            if (result) showSuccess("Tabela de logs atualizada!");
          });
        })
        .catch(() => {});
    })
    .catch(() => {
      showError("Erro na requisição dos sensores");
      return false;
    });
}

function setLogsList(userSelection) {
  return new Promise((resolve, reject) => {
    //Pegamos os logs do sensor/sensores
    getLogs(userSelection)
      .then((logsResult) => {
        //Atualizamos a tabela
        setLogsTable(logsResult);
        resolve(true);
      })
      .catch(() => {
        showError("Erro na requisição dos logs");
        reject(false);
      });
  });
}

function setLogsTable(logsList) {
  activeTable.clear();

  logsList.forEach(function (log) {
    activeTable.row.add([
      "<a>test</a>",
      log.sensor.tipo,
      log.timestamp,
      JSON.stringify(log),
    ]);
  });
  activeTable.draw();
}

function getCommand() {
  //Pegamos a lista de comandos do usuário
  const modalCommandsConfig = getCommandModalConfig();
  showModal(modalCommandsConfig, normalWidthModal)
    .then((commandResult) => {
      updateSensor(commandResult)
        .then((result) => {
          showSuccess("Comando enviado com sucesso!");
        })
        .catch(() => {
          showError("Erro na requisição do comando!");
          return false;
        });
    })
    .catch(() => {});
}
