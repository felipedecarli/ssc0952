//Table Stuff

var tableConfig = {
  responsive: true,
  bLengthChange: false,
  dom: '<"top">rt<"bottom"flp><"clear">',
  lengthChange: false,
  pageLength: 5,
  language: {
    url: "//cdn.datatables.net/plug-ins/1.10.24/i18n/Portuguese-Brasil.json",
  },
  ordering: false,
  columnDefs: [
    {
      className:
        "dt-center align-center justify-content-center align-items-center",
      targets: "_all",
    },
    {
      targets: -1,
      visible: false,
      searchable: false,
    },
    {
      targets: 0,
      render: function (data, type, row, meta) {
        console.log(row);
        if (type === "display") {
          data =
            '<a href="#" class="btn-blue btn-primary btn-lg btn-hover" onclick="handleRowClick(event)" data-cobject=' +
            row[3] +
            '><i class="bi bi-info-circle"></i></a>';
        }

        return data;
      },
    },
  ],
  search: {
    caseInsensitive: true,
  },
};

function setupLogsTable() {
  activeTable = $("#table-active").DataTable(
    $.extend({}, tableConfig, {
      //serverSide: true,
    })
  );

  $(".dataTables_filter").addClass("hidden");

  $("#myInputTextField").keyup(function () {
    activeTable.search($(this).val()).draw();
  });
}

function handleRowClick(e) {
  console.log(e.target);
  var thisElement;
  if (!e.target.hasAttribute("data-cobject")) {
    thisElement = e.target.parentElement;
  } else {
    thisElement = e.target;
  }
  const thisObj = JSON.parse(thisElement.getAttribute("data-cobject") + '"}');
  showDetails(thisObj);
  e.preventDefault();
}
