var style = {
  buttonsStyling: false,
  reverseButtons: true,
  customClass: {
    confirmButton: "btn btn-primary btn-lg btn-hover ",
    denyButton: "btn btn-primary btn-lg btn-hover ",
    cancelButton: "btn btn-primary btn-lg btn-hover ",
  },
};

var backChainingStyle = {
  allowOutsideClick: false,
  showCloseButton: true,
  showCancelButton: true,
  confirmButtonText: "Continuar",
  cancelButtonText: "Voltar",
};

const largeWidthModal = {
  didOpen: (value) => {
    $(".swal2-popup").css("width", "65em");
  },
  showClass: {
    popup: "animate__animated animate__fadeInDown animate__fast",
    icon: "animate__animated animate__heartBeat animate__delay-3s",
  },
  hideClass: {
    popup: "animate__animated animate__fadeOut animate__fast",
  },
};

const mediumWidthModal = {
  didOpen: (value) => {
    $(".swal2-popup").css("width", "45em");
  },
  showClass: {
    popup: "animate__animated animate__fadeInDown animate__fast",
    icon: "animate__animated animate__heartBeat animate__delay-3s",
  },
  hideClass: {
    popup: "animate__animated animate__fadeOut animate__fast",
  },
};

const normalWidthModal = {
  didOpen: (value) => {
    $(".swal2-popup").css("width", "35em");
  },
  showClass: {
    popup: "animate__animated animate__fadeInDown animate__fast",
    icon: "animate__animated animate__heartBeat animate__delay-3s",
  },
  hideClass: {
    popup: "animate__animated animate__fadeOut animate__fast",
  },
};

const alertsAnimation = {
  showClass: {
    icon: "animate__animated animate__heartBeat animate__delay-1s",
  },
};
function showError(msg) {
  return Swal.mixin(alertsAnimation).fire({
    title: "Oops!",
    text: msg,
    icon: "error",
    confirmButtonText: "Ok",
    buttonsStyling: false,
    customClass: {
      confirmButton: "btn-red btn-primary btn-lg btn-hover ",
    },
  });
}

function showSuccess(msg) {
  return Swal.mixin(alertsAnimation).fire({
    title: "Pronto!",
    text: msg,
    icon: "success",
    confirmButtonText: "Legal!",
    buttonsStyling: false,
    customClass: {
      confirmButton: "btn-green btn-primary btn-lg btn-hover ",
    },
  });
}

function showLoading() {
  return Swal.fire({
    title: false,
    html: '<div class="loading-animation"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>',
    allowOutsideClick: false,
    allowEscapeKey: false,
    showConfirmButton: false,
  });
}

function setupActiveButtons(list, multiple) {
  var myBtnsArea = $("." + list);
  myBtnsArea.click(function () {
    if (multiple) {
      $(this).toggleClass("active");
    } else {
      myBtnsArea.removeClass("active");
      $(this).addClass("active");
    }
  });
}

function getHtmlSearchText(objArray, displayAttribute, inputClass) {
  var htmlText = "";
  htmlText +=
    '<div><input type="search" id="' +
    inputClass +
    '_search" class="form-control" placeholder="Pesquise" autocomplete="new-password" /></div>';
  htmlText +=
    '<div class="form-group pt-3 pb-3"><div class="list-group list-group-style "><div class="list-group container-fluid" style="max-height: 300px; overflow-y: scroll; -webkit-overflow-scrolling: touch;">';
  objArray.forEach(function (thisObj) {
    htmlText +=
      '<button type="button" class="list-group-item list-group-item-action ' +
      inputClass +
      "\" value='" +
      thisObj[displayAttribute] +
      "' data-object='" +
      JSON.stringify(thisObj) +
      '\'><p class="text_job_area">' +
      thisObj[displayAttribute] +
      "</p></button>";
  });
  htmlText += "</div></div></div>";
  return htmlText;
}

function setupNormalizedSearch(list) {
  var myBtnsArea = document.querySelectorAll("." + list);
  $("#" + list + "_search").keyup(function () {
    var filter;
    filter = $(this)
      .val()
      .toUpperCase()
      .normalize("NFD")
      .replace(/[\u0300-\u036f]/g, "");
    myBtnsArea.forEach(function (btn) {
      var value = btn.value;
      if (
        value
          .toUpperCase()
          .normalize("NFD")
          .replace(/[\u0300-\u036f]/g, "")
          .indexOf(filter) > -1
      ) {
        btn.style.display = "block";
      } else {
        btn.style.display = "none";
      }
    });
  });
}

function getSearchableModalConfig(
  className,
  objList,
  displayAttribute,
  title,
  resultObjName,
  dismissible,
  multiple,
  required
) {
  var htmlText = getHtmlSearchText(objList, displayAttribute, className);
  var modalConfig = {
    title: title,
    html: htmlText,
    backdrop: dismissible,
    willOpen: () => {
      setupNormalizedSearch(className);
      setupActiveButtons(className, multiple);
    },
    preConfirm: (value) => {
      var selected = new Array();
      $("." + className).each(function (index, element) {
        var thisElement = $(element);
        if (thisElement.hasClass("active")) {
          selected.push(thisElement.data("object"));
        }
      });

      if (!selected || selected.length == 0) {
        if (required)
          Swal.showValidationMessage(
            '<i class="fa fa-info-circle"></i> ¡Debes seleccionar un valor! '
          );
        else return null;
      } else {
        var objReturn = {};
        objReturn[resultObjName] =
          selected.length == 1 ? selected[0] : selected;

        return selected.length == 0 ? null : objReturn;
      }
    },
  };
  return modalConfig;
}

function showDetails(logObj) {
  console.log(logObj);
  var htmlText = "";

  for (var [key, value] of Object.entries(logObj)) {
    if (key != "_id") {
      if (typeof value !== "object") {
        htmlText += '<div class="row"><div class="col-md-12"><h3>';
        htmlText += key + ": " + value;
        htmlText += "</h3></div></div>";
      } else {
        htmlText +=
          '<div class="row"><div class="col-md-12"><h3>Sensor</h3></div></div>';
        for (var [key2, value2] of Object.entries(value)) {
          htmlText += '<div class="row"><div class="col-md-12"><h5>';
          htmlText += key2 + ": " + value2;
          htmlText += "</h5></div></div>";
        }
        htmlText += "</p>";
      }
    }
  }

  Swal.fire({
    title: "Informações",
    html: htmlText,
    showCloseButton: true,
    showCancelButton: false,
    showConfirmButton: false,
    focusConfirm: false,
  });
}

function getCommandModalConfig() {
  var htmlText = "";
  htmlText +=
    '<form id="command_form" class="row g-3 needs-validation" novalidate>';
  htmlText += '    <div class="col-md-6">';
  htmlText +=
    '        <label for="tempMax" class="form-label">Temperatura máxima</label>';
  htmlText +=
    '        <input type="number" min="17" max="23" class="form-control" id="tempMax">';
  htmlText += '        <div class="valid-feedback">';
  htmlText += "            Valor válido!";
  htmlText += "        </div>";
  htmlText += '        <div class="invalid-feedback">';
  htmlText += "            Valor inválido! (17 a 23)";
  htmlText += "        </div>";
  htmlText += "    </div>";
  htmlText += '    <div class="col-md-6">';
  htmlText +=
    '        <label for="tempMin" class="form-label">Temperatura minima</label>';
  htmlText +=
    '        <input type="number" min="16" max="22" class="form-control" id="tempMin">';
  htmlText += '        <div class="valid-feedback">';
  htmlText += "            Valor válido!";
  htmlText += "        </div>";
  htmlText += '        <div class="invalid-feedback">';
  htmlText += "            Valor inválido! (16 a 22)";
  htmlText += "        </div>";
  htmlText += "    </div>";
  htmlText += '    <div class="col-md-6">';
  htmlText +=
    '        <label for="interval" class="form-label">Intervalo entre comandos</label>';
  htmlText +=
    '        <input type="number" min="1" max="120" class="form-control" id="interval">';
  htmlText += '        <div class="valid-feedback">';
  htmlText += "            Valor válido!";
  htmlText += "        </div>";
  htmlText += '        <div class="invalid-feedback">';
  htmlText += "            Valor inválido! (1 a 120)";
  htmlText += "        </div>";
  htmlText += "    </div>";
  htmlText += '    <div class="col-md-6">';
  htmlText +=
    '        <label for="temp" class="form-label">Temperatura</label>';
  htmlText +=
    '        <input type="number" min="16" max="23" class="form-control" id="temp">';
  htmlText += '        <div class="valid-feedback">';
  htmlText += "            Valor válido!";
  htmlText += "        </div>";
  htmlText += '        <div class="invalid-feedback">';
  htmlText += "            Valor inválido! (16 a 23)";
  htmlText += "        </div>";
  htmlText += "    </div>";
  htmlText += '    <div class="col-md-6">';
  htmlText += '        <label for="status" class="form-label">Status</label>';
  htmlText += '        <select class="form-select" id="status" required>';
  htmlText +=
    '            <option selected disabled value="">Escolha...</option>';
  htmlText += "            <option value=1>ON</option>";
  htmlText += "            <option value=0>OFF</option>";
  htmlText += "        </select>";
  htmlText += '        <div class="valid-feedback">';
  htmlText += "            Valor válido!";
  htmlText += "        </div>";
  htmlText += '        <div class="invalid-feedback">';
  htmlText += "            Valor inválido! (ON ou OFF)";
  htmlText += "        </div>";
  htmlText += "    </div>";
  htmlText += '    <div class="col-md-6">';
  htmlText +=
    '        <label for="emptyControl" class="form-label">Controle de presença</label>';
  htmlText += '        <select class="form-select" id="emptyControl" required>';
  htmlText +=
    '            <option selected disabled value="">Escolha...</option>';
  htmlText += "            <option value=1>ON</option>";
  htmlText += "            <option value=0>OFF</option>";
  htmlText += "        </select>";
  htmlText += '        <div class="valid-feedback">';
  htmlText += "            Valor válido!";
  htmlText += "        </div>";
  htmlText += '        <div class="invalid-feedback">';
  htmlText += "            Valor inválido! (ON ou OFF)";
  htmlText += "        </div>";
  htmlText += "    </div>";
  //htmlText += '    <div class="col-12">'
  //htmlText += '      <button class="btn btn-primary" type="submit">Enviar</button>'
  //htmlText += '    </div>'
  htmlText += "</form>";

  return {
    title: "Enviar comando",
    html: htmlText,
    showCloseButton: true,
    showCancelButton: false,
    showConfirmButton: true,
    confirmButtonText: "Enviar",
    focusConfirm: false,
    willOpen: function () {
      (function () {
        "use strict";

        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.querySelectorAll(".needs-validation");

        // Loop over them and prevent submission
        Array.prototype.slice.call(forms).forEach(function (form) {
          form.addEventListener(
            "submit",
            function (event) {
              if (!form.checkValidity()) {
                event.preventDefault();
                event.stopPropagation();
              }

              form.classList.add("was-validated");
            },
            false
          );
        });
      })();
    },
    preConfirm: () => {
      var form = $("#command_form");
      form.submit(function (e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        if ($(this).validate().form()) {
          return form.serialize();
        } else {
          Swal.showValidationMessage(
            '<i class="fa fa-info-circle"></i> Algum valor não é válido!'
          );
        }
      });
      form.submit();
    },
  };
}

function showBackchainModal(modalSteps) {
  const steps = [];
  for (let i = 1; i <= modalSteps.length; i++) {
    steps.push(i);
  }

  const swalQueueStep = Swal.mixin(
    $.extend({}, style, backChainingStyle, largeWidthModal, {
      progressSteps: steps,
    })
  );
  var currentStep = 0;
  var results = {};
  return (async () => {
    for (; currentStep < modalSteps.length; ) {
      Swal.close();
      const result = await swalQueueStep.fire(
        $.extend({}, modalSteps[currentStep], {
          currentProgressStep: currentStep,
        })
      );
      if (result.dismiss) {
        if (result.dismiss === Swal.DismissReason.cancel) {
          if (currentStep == 0) {
            return false;
          } else currentStep--;
        } else {
          return false;
        }
      } else {
        $.extend(results, result.value);
        currentStep++;
      }
    }
    if (currentStep === modalSteps.length) {
      return results;
    }
  })();
}

function showModal(modalConfig, sizeModalConfig) {
  return new Promise((resolve, reject) => {
    Swal.mixin($.extend({}, style, sizeModalConfig, modalConfig))
      .fire()
      .then(function (result) {
        if (result.dismiss) reject(result.dismiss);
        else resolve(result.value);
      });
  });
}
