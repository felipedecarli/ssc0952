const errorCheck = (resolve, reject) => {
  return {
    success: function (data) {
      Swal.close();
      console.log(data);
      resolve(data);
    },
    error: function (error) {
      console.log(error);
      Swal.close();
      reject();
    },
  };
};

function getSensors() {
  var settings = {
    url: _config.uri.url + _config.uri.sensors,
    method: "GET",
    crossDomain: false,
    dataType: "json",
    timeout: 0,
  };
  showLoading();
  return new Promise((resolve, reject) => {
    $.ajax($.extend({}, settings, errorCheck(resolve, reject)));
  });
}

function getLogs(id_sensor) {
  var settings = {
    url:
      _config.uri.url +
      _config.uri.logs +
      (id_sensor ? "?id_sensor=" + id_sensor : ""),
    method: "GET",
    crossDomain: true,
    dataType: "json",
    timeout: 0,
    headers: {
      accept: "application/json",
      "Access-Control-Allow-Origin": "*",
    },
  };
  showLoading();
  return new Promise((resolve, reject) => {
    $.ajax($.extend({}, settings, errorCheck(resolve, reject)));
  });
}

function updateSensor(commandObj) {
  var settings = {
    url: _config.uri.url + _config.uri.sensors,
    method: "PUT",
    crossDomain: false,
    dataType: "json",
    data: commandObj,
    timeout: 0,
  };
  showLoading();
  return new Promise((resolve, reject) => {
    $.ajax($.extend({}, settings, errorCheck(resolve, reject)));
  });
}
