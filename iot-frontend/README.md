# iot-frontend
Frontend Application for IoT project.

# Build and Run 
```
$ docker build -t iot-frontend .
$ docker run -it -d -p 80:80 iot-frontend
```
Application will be available in localhost:80