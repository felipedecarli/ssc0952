const createSensorObj = (topic) => {
  const sensorValues = topic.split('/');
  return { id: sensorValues[2], tipo: sensorValues[1], sala: sensorValues[0] };
};

module.exports = createSensorObj;
