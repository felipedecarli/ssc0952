/* eslint-disable no-restricted-syntax */
const { v4: uuidv4 } = require('uuid');
/*
        {
            tempMax: Int (17 a 23),
            tempMin: Int (16 a 22),
            interval: Int (1 a 120),
            temp: Int (16 a 23),
            status: Int (0 ou 1),
            emptyControl:Int (0 ou 1)
        }
    */
const createCommand = (commandObj) => {
  let sumCommands = 0;
  const response = {};
  for (const [key, value] of Object.entries(commandObj)) {
    if (key === 'tempMax') {
      sumCommands += 4;
      response['1'] = value;
    } else if (key === 'tempMin') {
      sumCommands += 8;
      response['2'] = value;
    } else if (key === 'interval') {
      sumCommands += 16;
      response['3'] = value;
    } else if (key === 'temp') {
      sumCommands += 32;
      response['4'] = value;
    } else if (key === 'status') {
      sumCommands += 1;
      response['21'] = value;
    } else if (key === 'emptyControl') {
      sumCommands += 2;
      response['22'] = value;
    }
  }
  response['0'] = sumCommands;
  response.s = new Date().toLocaleString();
  response['23'] = uuidv4();

  return response;
};
module.exports = createCommand;
