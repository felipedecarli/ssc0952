const createLogObj = (response, thisSensor) => ({
  sensor: thisSensor,
  valor: response['21'] || response.temp || response.umid || 'movimento',
  timestamp: response.s,
});

module.exports = createLogObj;
