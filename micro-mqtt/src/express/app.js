/* eslint-disable no-restricted-syntax */
const express = require('express');
const cors = require('cors');

const routes = {
  logs: require('./routes/logs'),
  sensors: require('./routes/sensors'),
};

const app = express();

app.use(cors());

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const makeHandlerAwareOfAsyncErrors = (handler) => async (req, res, next) => {
  try {
    await handler(req, res);
  } catch (error) {
    next(error);
  }
};

app.get('/health', (req, res) => {
  res.send('OK');
});

for (const [routeName, routeController] of Object.entries(routes)) {
  if (routeController.get) {
    app.get(
      `/api/${routeName}`,
      makeHandlerAwareOfAsyncErrors(routeController.get),
    );
  } else if (routeController.update) {
    app.put(
      `/api/${routeName}`,
      makeHandlerAwareOfAsyncErrors(routeController.update),
    );
  }
}

module.exports = app;
