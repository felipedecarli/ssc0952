module.exports = {
  getLogs: require('./get'),
  createLog: require('./create'),
};
