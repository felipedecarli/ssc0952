const mongoose = require('mongoose');
const logModel = require('../../../models/log.model');
const { createSensorObj, createLogObj } = require('../../../objects');

const Log = mongoose.model('Log', logModel);

const createLog = async (topic, message) => {
  const sensor = createSensorObj(topic);
  const log = createLogObj(message, sensor);
  Log.create(log, (err) => {
    console.log(err);
  });
};

module.exports = createLog;
