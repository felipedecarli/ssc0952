const mongoose = require('mongoose');
const logModel = require('../../../models/log.model');

const Log = mongoose.model('Log', logModel);

const getLogs = async (params) => {
  if (params) {
    return Log.find(params).exec();
  }
  return Log.find().exec();
};

module.exports = getLogs;
