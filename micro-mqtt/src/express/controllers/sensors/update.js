const Joi = require('joi');
const mqtt = require('mqtt');
const createRequestObj = require('../../../objects/command');
/*
        {
            tempMax: Int (17 a 23),
            tempMin: Int (16 a 22),
            interval: Int (1 a 120),
            temp: Int (16 a 23),
            status: Int (0 ou 1),
            emptyControl:Int (0 ou 1)
        }
*/
const schema = Joi.object().keys({
  tempMax: Joi.number().min(17).max(23),
  tempMin: Joi.number().min(16).max(22),
  interval: Joi.number().min(1).max(120),
  temp: Joi.number().min(16).max(23),
  status: Joi.number().valid(0, 1),
  emptyControl: Joi.number().valid(0, 1),
});

const updateSensor = async (req) => {
  const params = req.body;
  try {
    Joi.assert(params, schema);
  } catch (err) {
    return false;
  }
  const updateRequest = createRequestObj(params);
  const thisClient = mqtt.connect('mqtt://localhost');
  return thisClient.on('connect', () => {
    thisClient.subscribe('4/aircon/31', (err) => {
      if (!err) {
        thisClient.publish('4/aircon/31', updateRequest);
        return true;
      }
      return false;
    });
  });
};

module.exports = updateSensor;
