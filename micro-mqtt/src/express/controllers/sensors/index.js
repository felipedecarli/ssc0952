module.exports = {
  getSensors: require('./get'),
  updateSensor: require('./update'),
};
