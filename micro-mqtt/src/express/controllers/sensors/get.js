const mongoose = require('mongoose');
const sensorModel = require('../../../models/sensor.model');

const Sensor = mongoose.model('Sensor', sensorModel);

const getSensors = async (sensorId) => Sensor.find(sensorId ? { id: sensorId } : {}).exec();

module.exports = getSensors;
