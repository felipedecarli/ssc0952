const _ = require('lodash');
const { getLogs, createLog } = require('../controllers/logs');

const get = async (req, res) => {
  const queryParams = _.omitBy({
    'sensor.id': req.query.id_sensor,
    'sensor.sala': req.query.sala,
  }, _.isNil);
  const logs = await getLogs(queryParams);
  if (logs) {
    res.status(200).json(logs);
  } else {
    res.status(404).send('404 - Not found');
  }
};

const create = async (topic, message) => {
  const msg = JSON.parse(message.toString());
  await createLog(topic, msg);
};

module.exports = {
  get,
  create,
};
