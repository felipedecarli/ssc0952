const { getSensors, updateSensor } = require('../controllers/sensors');

const get = async (req, res) => {
  const sensorId = req.query.id;
  const sensors = await getSensors(sensorId);
  if (sensors.length > 0) {
    res.status(200).json(sensors);
  } else {
    res.status(404).send('404 - Not found');
  }
};

const update = async (req, res) => {
  const sensors = await updateSensor(req.body);
  if (sensors) {
    res.status(200).json({ status: 1 });
  } else {
    res.status(404).send('404 - Error');
  }
};

module.exports = {
  get,
  update,
};
