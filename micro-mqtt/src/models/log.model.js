const mongoose = require('mongoose');

const { Schema } = mongoose;

const sensorModel = require('./sensor.model');

const logObj = {
  sensor: sensorModel,
  valor: Number,
  timestamp: {
    type: String,
    required: true,
  },
};

const logSchema = new Schema(logObj);

module.exports = logSchema;
