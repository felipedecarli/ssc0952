const mongoose = require('mongoose');

const { Schema } = mongoose;

const sensorSchema = new Schema({
  id: {
    type: Number,
    required: true,
  },
  tipo: {
    type: String,
    required: true,
  },
  sala: {
    type: Number,
    required: true,
  },
});

module.exports = sensorSchema;
