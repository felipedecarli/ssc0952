const config = {
  env: process.env.ENVIRONMENT || 'development',
  port: process.env.PORT || '8080',
  mongo: {
    host: process.env.MONGO_HOST || 'mongodb://localhost:27017/iot',
  },
  mqtt: {
    host: process.env.MQTT_HOST || 'mqtt://localhost',
  },
};

module.exports = config;
