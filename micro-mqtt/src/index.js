const mongoose = require('mongoose');
const mqtt = require('mqtt');
const config = require('./config/config');
const app = require('./express/app');
const { create: createLog } = require('./express/routes/logs');

const mqttTopics = ['1/temp/27', '1/temp/28', '1/temp/29', '4/aircon', '1/movimento/32', '1/luz/33'];

const client = mqtt.connect(config.mqtt.host, {
  username: 'admin',
  password: 'admin',
});

mqttTopics.forEach((topic) => {
  client.subscribe(topic);
});

client.on('connect', () => {
  console.log(`MQTT connected on ${config.mqtt.host}`);
  client.on('message', (topic, message) => {
    console.log(`topic is ${topic} and message is ${message}`);
    createLog(topic, message);
  });
});

client.on('error', (err) => {
  console.log(err);
});

mongoose.connect(config.mongo.host).then(() => {
  if (process.env.NODE_ENV !== 'test') {
    console.log('Connected to %s', config.mongo.host);
    console.log('App is running ... \n');
    console.log('Press CTRL + C to stop the process. \n');
    app.listen(config.port, () => {
      console.info(`server started on port ${config.port} (${config.env})`);
    });
  }
})
  .catch((err) => {
    console.error('App starting error:', err.message);
    process.exit(1);
  });
