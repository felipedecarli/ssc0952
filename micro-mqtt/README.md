# micro-mqtt
Microservice connecting frontend application with RabbitMQ broker.

# Build and Run 
```
$ docker run -d -p 27017-27019:27017-27019 --name mongodb mongo:4.0.4
$ docker build -t micro-mqtt .
$ docker run --net=host --name=micro-mqtt micro-mqtt
```
API will be available in localhost:8080/api