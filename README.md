# IOT Final Project for SSC0952

## iot-frontend
Frontend Application for IoT project.

### Build and Run 
```
$ docker build -t iot-frontend .
$ docker run -it -d -p 80:80 iot-frontend
```
Application will be available in localhost:80

## micro-mqtt
Microservice connecting frontend application with RabbitMQ broker.

### Build and Run 
```
$ docker run -d -p 27017-27019:27017-27019 --name mongodb mongo:4.0.4
$ docker build -t micro-mqtt .
$ docker run --net=host --name=micro-mqtt micro-mqtt
```
API will be available in localhost:8080/api

## rabbitmq-mqtt-docker
RabbitMQ Dockerfile with default configuration to run MQTT Broker

### Build and Run 
```
$ docker build -t rmq-mqtt .
$ docker run -it \
    -p 15672:15672 -p 5672:5672 -p 1883:1883 \
    -v $PWD/docker/var/lib/rabbitmq:/var/lib/rabbitmq \
    rmq-mqtt
```
Admin interface will be available at localhost:15672 (admin:admin)
